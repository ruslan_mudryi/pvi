function addStudent(){
    var table = document.getElementById("myTable");
    var rowCount = table.rows.length;
    var row = table.insertRow(rowCount);
    var cellCount = table.rows[0].cells.length;
    for(var i = 0; i < cellCount; i++){
        var cell = row.insertCell(i);
        if(i == 0)
        {
            cell.innerHTML = '<th><input type = checkbox class = "largerCheckbox"></th>';
        }else if(i == 1)
        {
           cell.innerHTML = '<th>PZ-25</th>';
        }else if(i == 2)
        {
            cell.innerHTML = '<th>Viktor</th>';
        }else if(i == 3)
        {
            cell.innerHTML = '<th>M</th>';
        }else if(i == 4)
        {
            cell.innerHTML = '<th>06.11.2003</th>';
        }else if(i == 5)
        {
            cell.innerHTML = '<input type="radio"/>';
        }else{
            cell.innerHTML = '<button> <img src = ed.jpg /> <button onclick="deleteRow(this)"> <img src = del.jpg />';
        }
    }
}

function deleteRow(ele){
    var table = document.getElementById('myTable');
    var rowCount = table.rows.length;
    if(rowCount <= 1){
        alert("There is no row available to delete!");
        return;
    }
    if(ele){
        //delete specific row
        ele.parentNode.parentNode.remove();
    }else{
        //delete last row
        table.deleteRow(rowCount-1);
    }
}